import Vue from 'vue';
import VueRouter from 'vue-router';
import Login from '../views/Login.vue';
import Layout from '@/layout/Layout.vue';

const originalPush = VueRouter.prototype.push;
VueRouter.prototype.push = function push(location) {
  // @ts-ignore
    return originalPush.call(this, location).catch(err =>err);
};
Vue.use(VueRouter);
// 公共路由
export const constantRoutes = [
    {
        path: '',
        component: Layout,
        redirect: 'home',
        visible: true,
        children: [
            {
                path: 'home',
                component: () => import('@/views/Home.vue'),
                name: '首页',
                meta: {title: '首页', icon: 'el-icon-monitor', cache: true, affix: true},
            },
        ],
    },
    {
        path: '/login',
        name: '用户登录',
        visible: true,
        component: Login,
    },
    {
        path: '/user',
        name: '个人中心',
        visible: true,
        component: Layout,
        children: [
            {
                path: 'profile',
                visible: true,
                name: '个人信息',
                component: () => import('@/views/Profile.vue'),
                meta: {
                    title: '个人信息',
                    icon: '',
                },
            },
        ],
    }, {
        path: '/monitor',
        name: '监控管理',
        visible: true,
        component: Layout,
        children: [
            {
                path: 'jobLog',
                visible: true,
                name: '任务日志',
                component: () => import('@/views/JobLog.vue'),
                meta: {
                    title: '任务日志',
                    icon: '',
                },
            },
        ],
    },
    {
        path: '/404',
        name: '404',
        visible: true,
        meta: {title: '404'},
        component: () => import('@/views/404.vue'),
    },
];

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: constantRoutes,
});


export default router;
