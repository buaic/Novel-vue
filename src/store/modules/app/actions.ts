import {ActionTree} from 'vuex';
import {getProjectInfo} from '@/api/common';

const actions: ActionTree<any, any> = {
  async setCollapse({state, commit}, collapse: boolean) {
    commit('SET_COLLAPSE', collapse);
  },
  setProjectInfo({state, commit}) {
    getProjectInfo().then((response: any) => {
      if (response && response.data) {
        commit('SET_PROJECT_INFO', response.data);
      } else {
        commit('SET_PROJECT_INFO', {
          version: '0.0.1',
          name: 'novel',
          copyrightYear: '2020',
          copyrightCompany: 'cnovel.club',
          default: true,
        });
      }

    }).catch((error) => {
      commit('SET_PROJECT_INFO', {
        version: '0.0.1',
        name: 'novel',
        copyrightYear: '2020',
        copyrightCompany: 'cnovel.club',
        default: true,
      });
    });
  },

  setBrowserHeaderTitle({state, commit}, title: any) {
    commit('SET_BROWSER_HEADER_TITLE', title);
  },

  addVisitedView({commit}, view) {
    commit('ADD_VISITED_VIEW', view);
  },
  addView({dispatch}, view) {
    dispatch('addVisitedView', view);
    dispatch('addCachedView', view);
  },
  addCachedView({commit}, view) {
    commit('ADD_CACHED_VIEW', view);
  },
  updateVisitedView({commit}, view) {
    commit('UPDATE_VISITED_VIEW', view);
  },
  delCachedView({commit, state}, view) {
    return new Promise((resolve) => {
      commit('DEL_CACHED_VIEW', view);
      resolve([...state.cachedViews]);
    });
  },

  delView({dispatch, state}, view) {
    return new Promise((resolve) => {
      dispatch('delVisitedView', view);
      dispatch('delCachedView', view);
      resolve({
        visitedViews: [...state.visitedViews],
        cachedViews: [...state.cachedViews],
      });
    });
  },
  delVisitedView({commit, state}, view) {
    return new Promise((resolve) => {
      commit('DEL_VISITED_VIEW', view);
      resolve([...state.visitedViews]);
    });
  },

  delOthersViews({dispatch, state}, view) {
    return new Promise((resolve) => {
      dispatch('delOthersVisitedViews', view);
      dispatch('delOthersCachedViews', view);
      resolve({
        visitedViews: [...state.visitedViews],
        cachedViews: [...state.cachedViews],
      });
    });
  },
  delOthersVisitedViews({commit, state}, view) {
    return new Promise((resolve) => {
      commit('DEL_OTHERS_VISITED_VIEWS', view);
      resolve([...state.visitedViews]);
    });
  },
  delOthersCachedViews({commit, state}, view) {
    return new Promise((resolve) => {
      commit('DEL_OTHERS_CACHED_VIEWS', view);
      resolve([...state.cachedViews]);
    });
  },
  delAllViews({dispatch, state}, view) {
    return new Promise((resolve) => {
      dispatch('delAllVisitedViews', view);
      dispatch('delAllCachedViews', view);
      resolve({
        visitedViews: [...state.visitedViews],
        cachedViews: [...state.cachedViews],
      });
    });
  },
  delAllVisitedViews({commit, state}) {
    return new Promise((resolve) => {
      commit('DEL_ALL_VISITED_VIEWS');
      resolve([...state.visitedViews]);
    });
  },
  delAllCachedViews({commit, state}) {
    return new Promise((resolve) => {
      commit('DEL_ALL_CACHED_VIEWS');
      resolve([...state.cachedViews]);
    });
  },
  toggleDevice({commit}, device) {
    commit('TOGGLE_DEVICE', device);
  },
};

export default actions;
