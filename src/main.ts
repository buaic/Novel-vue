import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import ElementUI from 'element-ui';
import ElTreeSelect from 'el-tree-select';
import elementUIVerify from 'element-ui-verify';
import eIconPicker from 'e-icon-picker';
import 'e-icon-picker/dist/index.css';
import 'e-icon-picker/dist/main.css';
import axios from '@/utils/http';
import VueCropper from 'vue-cropper';
// collapse 展开折叠
import CollapseTransition from 'element-ui/lib/transitions/collapse-transition';
import 'element-ui/lib/theme-chalk/index.css';
// fade/zoom 等
import 'element-ui/lib/theme-chalk/base.css';
import '@/assets/css/index.scss';
import '@/router/permission';
import '@/directive/permission';
import 'default-passive-events';
import utils from '@/utils';
import resetForm = utils.resetForm;
import download = utils.download;

Vue.config.productionTip = false;

Vue.use(VueCropper);
Vue.prototype.resetForm = resetForm;
Vue.prototype.$download = download;
Vue.prototype.$axios = axios;
Vue.component(CollapseTransition.name, CollapseTransition);
Vue.use(ElementUI);
Vue.use(ElTreeSelect);
Vue.use(eIconPicker);
Vue.use(elementUIVerify); // ui校验插件 https://aweiu.com/documents/element-ui-verify/

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
