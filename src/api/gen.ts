import http from '@/utils/http';

/**
 * 获取数据表信息
 * @param params
 * @returns {*|Promise<any>}
 */
export function getGenList(params?: any) {
  if (params) {
    delete params.orderByColumn;
    delete params.isAsc;
  }
  return http.get('tool/gen/list', {params});
}

/**
 * 生成表代码
 * @param params
 * @returns {*|Promise<any>}
 */
export function genCode(params?: any) {
  return http.get('tool/gen/genCode/' + params.tableName, {responseType: 'blob'});
}


/**
 * 批量生成表代码
 * @param params
 * @returns {*|Promise<any>}
 */
export function batchGenCode(params: any) {
  return http.get('tool/gen/batchGenCode', {params, responseType: 'blob'});
}
