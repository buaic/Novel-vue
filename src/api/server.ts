import http from '@/utils/http';

/**
 * 获取系统运行信息
 * @param params
 * @returns {*|Promise<any>}
 */
export function getServerInfo(params?: any) {
  if (!params) {
    params = {};
  }
  params.loading = true;
  return http.get('monitor/server', {params});
}
