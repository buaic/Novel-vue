import http from '@/utils/http';

/**
 * 获取调度日志列表
 * @param params
 * @returns {*|Promise<any>}
 */
export function getJobLogList(params?: any) {
  return http.get('monitor/jobLog/list', {params});
}


/**
 * 删除任务调度日志
 * @param params
 * @returns {*|Promise<any>}
 */
export function removeJobLog(params: any) {
  return http.delete('monitor/jobLog/remove', {params});
}

/**
 * 清空所有任务日志
 * @returns {*|Promise<any>}
 */
export function cleanJobLogs() {
  return http.delete('monitor/jobLog/clean');
}

/**
 * 导出任务调度日志信息
 * @param params
 * @returns {*|Promise<any>}
 */
export function exportJobLog(params?: any) {
  if (!params) {
    params = {};
  }
  return http.get('monitor/jobLog/export', {params});
}
