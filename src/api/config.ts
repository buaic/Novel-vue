import http from '@/utils/http';

/**
 * 获取参数配置列表
 * @param params
 * @returns {*|Promise<any>}
 */
export function getConfigList(params?: any) {
    return http.get('system/config/list', {params: params});
}


/**
 * 删除参数配置
 * @param params
 * @returns {*|Promise<any>}
 */
export function removeConfig(params: any) {
    return http.delete('system/config/remove', {params: params});
}

/**
 * 保存参数配置
 * @param params
 * @returns {*|Promise<any>}
 */
export function updateConfig(params: any) {
    return http.put('system/config/edit', params);
}

/**
 * 新增参数配置
 * @param params
 * @returns {*|Promise<any>}
 */
export function addConfig(params: any) {
    return http.post("system/config/add", params);
}

/**
 * 验证参数key是否唯一
 * @param params
 * @returns {*|Promise<any>}
 */
export function checkConfigKeyUnique(params: any) {
    params.loading = false;
    return http.post('system/config/checkConfigKeyUnique', params);
}

/**
 * 导出参数信息
 * @param params
 * @returns {*|Promise<any>}
 */
export function exportConfig(params?: any) {
    if (!params) {
        params = {};
    }
    return http.get('system/config/export', {params});
}

/**
 * 根据参数键名查询参数值
 * @param params
 * @returns {*|Promise<any>}
 */
export function getConfigKey(params: any) {
    return http.get('system/config/' + params.configKey);
}
