import {ElForm} from 'element-ui/types/form';
import {BindingResultObject, Result} from '*.vue';

const baseURL = process.env.VUE_APP_BASE_API;

// 表单重置
export function resetForm(this: any, refName: string): void {
  if (this.$refs[refName] !== undefined) {
    this.$refs[refName].resetFields();
  }
}

export function isMobile() {
  return navigator.userAgent.match(/(phone|pad|pod|iPhone|iPod|ios|iPad|Android|Mobile|BlackBerry|IEMobile|MQQBrowser|JUC|Fennec|wOSBrowser|BrowserNG|WebOS|Symbian|Windows Phone)/i);
}

// 通用下载方法
// export function download(fileName: string): void {
//  let url=   baseURL + "/resources/download?fileName=" + encodeURI(fileName);
//     window.open(url,'_blank')
// }
// 通用下载方法
export function download(fileName) {
  window.location.href = baseURL + '/resources/download?fileName=' + encodeURI(fileName) + '&_=' + new Date().getTime();
}


export const mimeMap = {
  xlsx: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
  zip: 'application/zip',
};

/**
 * 解析blob响应内容并下载
 * @param {*} res blob响应内容
 * @param {String} mimeType MIME类型
 */
export function resolveBlob(res, mimeType) {
  const aLink = document.createElement('a');
  const blob = new Blob([res.data], {type: mimeType});
  // //从response的headers中获取filename, 后端response.setHeader("Content-disposition", "attachment; filename=xxxx.docx") 设置的文件名;
  const patt = new RegExp('filename=([^;]+\\.[^.;]+);*');
  const contentDisposition = decodeURI(res.headers['content-disposition']);
  const result = patt.exec(contentDisposition);
  if (result) {
    let fileName = result[1];
    fileName = fileName.replace(/"/g, '');
    aLink.href = URL.createObjectURL(blob);
    aLink.setAttribute('download', fileName); // 设置下载文件名称
    document.body.appendChild(aLink);
    aLink.click();
    document.body.appendChild(aLink);
  }
}


/**
 * 过滤对象参数为空或者null的属性
 * 这个方法不会影响原来的对象，而是返回一个新对象
 * @param obj
 */
export function filterParams(obj: {}) {
  const _newPar = {};
  for (const key in obj) {
    //如果对象属性的值不为空，就保存该属性（这里我做了限制，如果属性的值为0，保存该属性。如果属性的值全部是空格，属于为空。）
    if ((obj[key] === 0 || obj[key] === false || obj[key]) && obj[key].toString().replace(/(^\s*)|(\s*$)/g, '') !== '') {
      _newPar[key] = obj[key];
    }
  }
  return _newPar;
}

/**
 * 回显后端表单校验错误结果
 * @param result 后端错误信息
 * @param form 表单对象
 * @param rule 校验函数
 */
export function validateForm(result: Result, form: ElForm, rule: any) {
  if (result.code === 400) {
    const errMessage: BindingResultObject[] = result.data;
    if (errMessage && errMessage.length > 0) {
      const rules = JSON.parse(JSON.stringify(rule));
      const props: string[] = [];
      errMessage.forEach((item, index) => {
        props.push(item.field);
        rule[item.field].push({
          // js新增一个自定义校验
          validator: (rule, value, callback) => {
            callback(new Error(item.message ? item.message : '未知错误'))
          },
          trigger: 'blur'
        });
      });
      form.validateField(props, function (errorMessage: string) {
        console.log(errorMessage)
      }); // 手动校验

      errMessage.forEach((item, index) => {
        rule[item.field] = rules[item.field];
      })
    }
  }
}
